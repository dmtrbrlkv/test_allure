import os

current_verion = os.environ.get('APK_VERSION')
print(f'current version: {current_verion}')

html = """
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>APK Versions</title>
</head>
<body>
"""

all_version = []
for d in os.listdir('public'):
    print(d)
    if os.path.exists(os.path.join('public', d, 'index.html')):
        all_version.append(d)

print(all_version)

if current_verion not in all_version:
    all_version.append(current_verion)

all_version.sort()
html = html + '  <ul>\n'
for v in all_version:
    html = html + f"    <li><a href='{v}'>{v}</a></li>\n"
html = html + '  </ul>\n'

html = html + """
</body>
</html>
"""

with open('public/index.html', mode='w') as index:
    index.write(html)
