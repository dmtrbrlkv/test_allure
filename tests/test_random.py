import random

import allure


@allure.feature('Может упадет, а может нет')
def test_rnd():
    with allure.step('Первый шаг'):
        a = 1
        b = random.randint(0, 1)
    with allure.step('Шаг два'):
        assert a == b