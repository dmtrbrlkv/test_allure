import allure


@allure.feature('Сравнение чисел')
def test_foo():
    with allure.step('Первый шаг'):
        a = 1
        b = 1
    with allure.step('Шаг два'):
        assert a == b


@allure.feature('Сравнение строк')
def test_bar():
    with allure.step('Первый шаг'):
        a = '1'
        b = '1'
    with allure.step('Шаг два'):
        assert a == b